var parent = document.parent;
var date1 = new Date();
var year = date1.getFullYear();
var month = 1 + date1.getMonth();
var date = date1.getDate();

if (month < 10) {
    month = "0" + month;
}
// if (date < 10) {
//     date = "0" + date;
// }


var yearPath = parent.childByNamePath(year);

if(yearPath == null) {
	yearPath = parent.createFolder(year);
}

var monthPath = yearPath.childByNamePath(month);

if(monthPath == null) {
	monthPath = yearPath.createFolder(month);
}

// var datePath = monthPath.childByNamePath(date);

// if(datePath == null) {
// 	datePath = monthPath.createFolder(date);
// }

// var file = datePath.childByNamePath(document.getName());

// if(file != null ) {
// 	var timestamp = Math.round(new Date().getTime()/1000);
// 	document.setName(timestamp + "-" + document.getName());
// }

// document.move(datePath);